# -*- coding: utf-8 -*-

{
    'name': 'Mail Filter',
    'category': 'mail',
    'version': '10.0.2018.09.07-5',
    'depends': ['mail'],
    'data': [
        'view/mail_filter_views.xml'
    ]
}
