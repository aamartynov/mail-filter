# -*- coding: utf-8 -*-

import re
from logging import getLogger
from email.header import decode_header
from lxml.html import document_fromstring

from odoo import models, fields, api, tools
from odoo.tools import ustr


logger = getLogger(__name__)


class MailTread(models.AbstractModel):
    _inherit = 'mail.thread'

    @api.model
    def message_route(self, message, message_dict, model=None, thread_id=None, custom_values=None):
        lookup_message = {key: tools.decode_smtp_header(value) for key, value in message.items()}

        # mail body access for keys `Body.Text` and `Body.Html`
        lookup_message.update({
            'Body.Text': document_fromstring(message_dict.get('body')).text_content(),
            'Body.Html': message_dict.get('body')
        })

        for configuration in self.env['mail_filter.configuration'].search([('enable', '=', True)]):

            predicates = [self._compute_predicate(lookup_message, _.key, _.value) for _ in configuration.predicate_ids.filtered('enable')]
            
            if configuration.mode == 'any':
                ignore = any(predicates)
            elif configuration.mode == 'all':
                ignore = all(predicates)
            else:
                ignore = False

            if ignore:
                logger.info(u'mail with subject {} will ignore'.format(lookup_message.get('Subject')))
                return []

        return super(MailTread, self).message_route(message, message_dict, model, thread_id, custom_values)

    @api.model
    def _compute_predicate(self, message, search_key, search_value):
        for key, value in message.items():
            try:
                if re.match(search_key, key) is not None:
                    return re.match(search_value, value) is not None
            except Exception as error:
                logger.exception(error)

        return False

