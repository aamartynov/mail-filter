# -*- coding: utf-8 -*-

from logging import getLogger

from odoo import models, fields, api


logger = getLogger(__name__)


class MailFilterPredicate(models.Model):
    _name = 'mail_filter.predicate'

    name = fields.Char(string='Name', related='key')
    key = fields.Char(string='Key', default='')
    value = fields.Char(string='Value', default='')
    enable = fields.Boolean(string='Enable', default=True)
    configuration_id = fields.Many2one('mail_filter.configuration', string='Configuration')
