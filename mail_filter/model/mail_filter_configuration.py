# -*- coding: utf-8 -*-
"""
For autoresponders config see - https://github.com/jpmckinney/multi_mail/wiki/Detecting-autoresponders
"""

from logging import getLogger

from odoo import models, fields, api


logger = getLogger(__name__)


class MailFilterConfiguration(models.Model):
    _name = 'mail_filter.configuration'

    name = fields.Char(string='Name')
    mode = fields.Selection(string='Mode', selection=[
        ('all', 'All'),
        ('any', 'Any')
    ], default='any')
    enable = fields.Boolean(string='Enable', default=True)
    predicate_ids = fields.One2many('mail_filter.predicate', 'configuration_id', string='Predicate')
